import time
import selenium
from selenium import webdriver, common
from urllib.parse import urlparse
import itertools as it
import pandas as pd

# def getName(url):
#     o = urlparse(url)
#     name = [i for i in o.path.split('/') if i]
#     return name[1]

def handleError(att):
    try:
        return driver.find_element_by_css_selector(att).text
    except selenium.common.exceptions.NoSuchElementException:
        return 'Not Found'

driver = webdriver.Chrome(executable_path='/Users/thaihuy/Downloads/chromedriver')

driver.get('https://hellobacsi.com/thuoc/')
number_row = 2

title_list = []
author_list = []
medical_consultation_list = []
name_ingredient_1 = []
name_ingredient_2 = []
name_ingredient_3 = []
img_path_list = []
tim_hieu_chung = []
tac_dung = []
lieu_dung = []
cach_dung = []
tac_dung_phu = []
than_trong_canh_bao = []
tuong_tac_thuoc = []
bao_quan_thuoc = []
dang_bao_che = []
khan_cap_qua_lieu = []
tt_tkd = [] #than trong truoc khi dung
th_kc_ql = [] #truong hop khan cap qua lieu
dieu_can_than_trong = []
currentURL = []

#F
for i in range(2, 34):
    time.sleep(2)
    parent = '#section-f > div:nth-child({0}) >'.format(i)
    #section-f > div:nth-child(2) > div:nth-child(1) > a
    #section-f > div:nth-child(33) > div:nth-child(4) > a
    for x in range(1, 5):
        time.sleep(2)
        url = driver.find_element_by_css_selector(parent + 'div:nth-child({0}) > a'.format(x)).get_attribute('href')
        currentURL.append(url)
        print(url)
        if url is not None:
            driver.get(url)

        time.sleep(2)

        title_list.append(handleError('#article > div > div.hc2-item-col.hc2-item-col-8.hc2-item-no-margin > div.entry-header > h1'))
        author_list.append(handleError('#article > div > div.hc2-item-col.hc2-item-col-8.hc2-item-no-margin > div.entry-header > p'))
        medical_consultation_list.append(handleError('#article > div > div.hc2-item-col.hc2-item-col-8.hc2-item-no-margin > div.entry-content > div.hhg-drug-info'))
        name_ingredient_1.append(handleError('#article > div > div.hc2-item-col.hc2-item-col-8.hc2-item-no-margin > div.entry-content > div.entry-content-body > p:nth-child(2)'))
        name_ingredient_2.append(handleError('#article > div > div.hc2-item-col.hc2-item-col-8.hc2-item-no-margin > div.entry-content > div.entry-content-body > p:nth-child(3)'))
        name_ingredient_3.append(handleError('#article > div > div.hc2-item-col.hc2-item-col-8.hc2-item-no-margin > div.entry-content > div.entry-content-body > p:nth-child(4)'))
        try:
            img_path_list.append(driver.find_element_by_css_selector('#article > div > div.hc2-item-col.hc2-item-col-8.hc2-item-no-margin > div.entry-content > div.hc2-content-single-featured-image.fade-in > img').get_attribute("src"))
        except selenium.common.exceptions.NoSuchElementException:
            img_path_list.append('Not Found')

        tim_hieu_chung.append(handleError('#h-tim-hieu-chung'))
        tac_dung.append(handleError('#h-tac-dung'))
        lieu_dung.append(handleError('#h-lieu-dung'))
        cach_dung.append(handleError('#h-cach-dung'))
        tac_dung_phu.append(handleError('#h-tac-dung-phu'))
        than_trong_canh_bao.append(handleError('#h-than-trong-canh-bao'))
        tuong_tac_thuoc.append(handleError('#h-tuong-tac-thuoc'))
        bao_quan_thuoc.append(handleError('#h-bao-quan-thuoc'))
        dang_bao_che.append(handleError('#h-dang-bao-che'))
        khan_cap_qua_lieu.append(handleError('#h-khan-cap-qua-lieu'))
        th_kc_ql.append(handleError('#h-truong-hop-khan-cap-qua-lieu'))
        tt_tkd.append(handleError('#h-than-trong-truoc-khi-dung'))
        dieu_can_than_trong.append(handleError('#h-dieu-can-than-trong'))

        # with open(f'/Users/thaihuy/Developer/amber_crawler_selenium/txt_thuoc_hellobacsi/Z/{name}.txt', 'a') as file:
        #     for v in detail.values():
        #         file.write(v + "\n\n")
        df = pd.DataFrame(list(it.zip_longest(
                                title_list,
                                author_list,
                                medical_consultation_list,
                                name_ingredient_1,
                                name_ingredient_2,
                                name_ingredient_3,
                                img_path_list,
                                currentURL,
                                tim_hieu_chung,
                                tac_dung,
                                lieu_dung,
                                cach_dung,
                                tac_dung_phu,
                                than_trong_canh_bao,
                                tuong_tac_thuoc,
                                bao_quan_thuoc,
                                dang_bao_che,
                                khan_cap_qua_lieu,
                                th_kc_ql,
                                tt_tkd,
                                dieu_can_than_trong,
                            )),
                        columns=['title',
                                 'author',
                                 'medical_consultation',
                                 'name_ingredient_1',
                                 'name_ingredient_2',
                                 'name_ingredient_3',
                                 'img_path',
                                 'origin_link',
                                 'tim_hieu_chung',
                                 'tac_dung',
                                 'lieu_dung',
                                 'cach_dung',
                                 'tac_dung_phu',
                                 'than_trong_canh_bao',
                                 'tuong_tac_thuoc',
                                 'bao_quan_thuoc',
                                 'dang_bao_che',
                                 'khan_cap_qua_lieu',
                                 'truong_hop_khan_cap_qua_lieu',
                                 'than_trong_truoc_khi_dung',
                                 'dieu_can_than_trong',
                                 ])
        df.to_csv('F.csv', index=False)
        print('------------------------------------------')

        time.sleep(2)
        driver.get('https://hellobacsi.com/thuoc/')

    print("____ " + "Number Row: " + str(number_row) + " ____")
    number_row += 1

driver.quit()