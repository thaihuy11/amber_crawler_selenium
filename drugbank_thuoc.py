import time
import selenium
from selenium import webdriver, common
import os
import urllib
from urllib import error
from urllib.parse import urlparse
from requests import get, exceptions
from urllib.request import urlretrieve
import pandas as pd
import itertools as it

def getNamePdf(url):
    name = ''

    try:
        response = get(url)
        file_name = os.path.basename(urlparse(response.url).path)
        name = file_name
        print(name)
    except exceptions.ChunkedEncodingError:
        print("pdf file is error")

    return name

def getNameImage(url):
    name_img = ''

    try:
        response = get(url)
        file_name = os.path.basename(urlparse(response.url).path)
        name_img = file_name
        print(name_img)
    except exceptions.ChunkedEncodingError:
        print("image is error")

    return name_img

# chrome_options = webdriver.ChromeOptions()
# chrome_options.add_argument('--headless')

driver = webdriver.Chrome(executable_path='/Users/thaihuy/Downloads/chromedriver')

ten = []
sodangky = []
phanloai = []
hoatchat = []
dangbaoche = []
qcdonggoi = []
tuoitho = []
tieuchuan = []
ctysx = []
ctydk = []
giabuonbankekhai = []
table = []
isPdf = []
isImage = []
currentURL = []

number_page = 229

css_list = [
    'body > jhi-main > jhi-chi-tiet-thuoc > div > h2', #ten
    'body > jhi-main > jhi-chi-tiet-thuoc > div > div.card-body > div > div.row.justify-content-center.my-3 > div:nth-child(2) > dl > dd:nth-child(2) > span', #sodangky
    'body > jhi-main > jhi-chi-tiet-thuoc > div > div.card-body > div > div.row.justify-content-center.my-3 > div:nth-child(2) > dl > dd:nth-child(4) > span', #phanloai
    'body > jhi-main > jhi-chi-tiet-thuoc > div > div.card-body > div > div.row.justify-content-center.my-3 > div:nth-child(2) > dl > dd:nth-child(6) > ul', #hoatchat
    'body > jhi-main > jhi-chi-tiet-thuoc > div > div.card-body > div > div.row.justify-content-center.my-3 > div:nth-child(2) > dl > dd:nth-child(8) > span', #dangbaoche
    'body > jhi-main > jhi-chi-tiet-thuoc > div > div.card-body > div > div.row.justify-content-center.my-3 > div:nth-child(2) > dl > dd:nth-child(10) > span', #qcdonggoi
    'body > jhi-main > jhi-chi-tiet-thuoc > div > div.card-body > div > div.row.justify-content-center.my-3 > div:nth-child(2) > dl > dd:nth-child(12) > span', #tuoitho
    'body > jhi-main > jhi-chi-tiet-thuoc > div > div.card-body > div > div.row.justify-content-center.my-3 > div:nth-child(2) > dl > dd:nth-child(14) > span', #tieuchuan
    'body > jhi-main > jhi-chi-tiet-thuoc > div > div.card-body > div > div.row.justify-content-center.my-3 > div:nth-child(2) > dl > dd:nth-child(16)', #ctysx
    'body > jhi-main > jhi-chi-tiet-thuoc > div > div.card-body > div > div.row.justify-content-center.my-3 > div:nth-child(2) > dl > dd:nth-child(18)', #ctydk
    'body > jhi-main > jhi-chi-tiet-thuoc > div > div.card-body > div > div.row.justify-content-center.my-3 > div:nth-child(2) > div > p > small', #giabuonbankekhai
    'body > jhi-main > jhi-chi-tiet-thuoc > div > div.card-body > div > div.row.justify-content-center.my-3 > div:nth-child(2) > div > div > table > tbody', #table
    'body > jhi-main > jhi-chi-tiet-thuoc > div > div.card-body > div > div.row.justify-content-center.my-3 > div:nth-child(1) > div > div > img', #image
    'body > jhi-main > jhi-chi-tiet-thuoc > div > div.card-body > div > div:nth-child(2) > div > div > div > iframe', #pdf
]

for i in range(229, 351):
    time.sleep(4)
    driver.get("https://drugbank.vn/danh-sach-thuoc?page={0}&size=20&sort=tenThuoc,asc".format(i))

    url_page = driver.current_url

    try:
        for x in range(1, 21):
            # select button xem
            time.sleep(3)
            button = driver.find_element_by_css_selector(
                'body > jhi-main > jhi-danh-sach-thuoc > div.card.border-0 > div.card-body > div > table > tbody > tr:nth-child({0}) > td.text-right > div > button'.format(x))
            if button is not None:
                button.click()

            time.sleep(3)

            ten.append(driver.find_element_by_css_selector(css_list[0]).text)
            print(driver.find_element_by_css_selector(css_list[0]).text)

            sodangky.append(driver.find_element_by_css_selector(css_list[1]).text)
            print(driver.find_element_by_css_selector(css_list[1]).text)

            phanloai.append(driver.find_element_by_css_selector(css_list[2]).text)

            hoatchat.append(driver.find_element_by_css_selector(css_list[3]).text)

            dangbaoche.append(driver.find_element_by_css_selector(css_list[4]).text)

            qcdonggoi.append(driver.find_element_by_css_selector(css_list[5]).text)

            tuoitho.append(driver.find_element_by_css_selector(css_list[6]).text)

            tieuchuan.append(driver.find_element_by_css_selector(css_list[7]).text)

            ctysx.append(driver.find_element_by_css_selector(css_list[8]).text)

            ctydk.append(driver.find_element_by_css_selector(css_list[9]).text)

            try:
                giabuonbankekhai.append(driver.find_element_by_css_selector(css_list[10]).text)
            except selenium.common.exceptions.NoSuchElementException:
                giabuonbankekhai.append("Đang cập nhật")

            try:
                table.append(driver.find_element_by_css_selector(css_list[11]).text)
            except selenium.common.exceptions.NoSuchElementException:
                table.append("Đang cập nhật")

            time.sleep(4)
            # image path
            try:
                img_path = driver.find_element_by_css_selector(css_list[12]).get_attribute('src')

                local_data_dir_img = '/Users/thaihuy/Developer/amber-crawler/drugstore/img/'
                img_name = getNameImage(img_path)
                local_path_img = os.path.join(local_data_dir_img, img_name)

                isImage.append(img_path)
                urlretrieve(img_path, local_path_img)
                print("call img path")
            except selenium.common.exceptions.NoSuchElementException:
                isImage.append("Image not found")
                print("Image not found")
            except urllib.error.HTTPError or urllib.error.URLError:
                print("Disconnect")

            # pdf path
            try:
                pdf_path = driver.find_element_by_css_selector(css_list[13]).get_attribute('src')

                local_data_dir_pdf = '/Users/thaihuy/Developer/amber-crawler/drugstore/pdf/'
                pdf_name = getNamePdf(pdf_path)
                local_path_pdf = os.path.join(local_data_dir_pdf, pdf_name)

                isPdf.append(pdf_path)
                urlretrieve(pdf_path, local_path_pdf)
                print("call pdf path")
            except selenium.common.exceptions.NoSuchElementException:
                isPdf.append("pdf not found")
                print("pdf not found")
            except urllib.error.HTTPError or urllib.error.URLError:
                print("Disconnect")

            # original link
            url = driver.current_url
            currentURL.append(url)
            print(url)


            df = pd.DataFrame(list(it.zip_longest(
                                       ten,
                                       sodangky,
                                       phanloai,
                                       hoatchat,
                                       dangbaoche,
                                       qcdonggoi,
                                       tuoitho,
                                       tieuchuan,
                                       ctysx,
                                       ctydk,
                                       giabuonbankekhai,
                                       table,
                                       isPdf,
                                       isImage,
                                       currentURL
                                       )),
                              columns=['Ten',
                                       'So dang ky',
                                       'Phan loai',
                                       'Hoat Chat - Nong do/Ham luong',
                                       'Dang bao che',
                                       'Quy cach dong goi',
                                       'Tuoi tho',
                                       'Tieu chuan',
                                       'Cong ty san xuat',
                                       'Cong ty dang ky',
                                       'giabuonbankekhai',
                                       'Table',
                                       'Pdf link',
                                       'Image link',
                                       'Original link'
                                       ])
            df.to_csv('drugbank_512.csv', index=False)
            print('------------------------------------------')

            # back
            time.sleep(2)
            driver.get(url_page)

        print("____ " + "Number Page: " + str(number_page) + " ____")
        number_page += 1

    except selenium.common.exceptions.StaleElementReferenceException:
        print("++++++++++++++++++++++++++++++++++++++++++")

driver.quit()
